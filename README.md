
<div>

- I'm **Back End Developer** in development.
- Graduating in Software Engineering at Pitágoras Ampli.
- Graduate in Financial Management at Fundação Santo André.

  
Technologies:
- Javascript, Typescript
- NodeJs, NestJs
- Git and Github, Docker, AWS etc.

</div>

</br>
  
<div>

  <a target="_blanck"> [![Linkedin](https://img.shields.io/badge/-LinkedIn-1d1f21?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/guilhermelinosx)</a>
  <img src="https://komarev.com/ghpvc/?username=guilhermelinosx&style=for-the-badge&color=1d1f21"/>
  
</div>

</br>

<div align="center">

<img height="150" src="https://github-readme-stats.vercel.app/api?username=guilhermelinosx&theme=dark&hide_border=true&include_all_commits=true&count_private=true&text_color=fff&icon_color=fff&title_color=fff&bg_color=0d1117&show_icons=true">
<img height="150" src="https://github-readme-stats.vercel.app/api/top-langs/?username=guilhermelinosx&theme=dark&hide_border=true&include_all_commits=true&count_private=true&layout=compact&text_color=fff&icon_color=fff&title_color=fff&bg_color=0d1117&show_icons=true">

</div>
